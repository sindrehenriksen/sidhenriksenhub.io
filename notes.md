---
layout: page
title: Notes
permalink: /misc/
---

## Course notes and code
---

### Deep Learning (Coursera, Andrew Ng)

#### Course 1
- Week 1 - Introduction and background (no notes)
- [Week 2 - Logistic regression as a neural network](/notes/coursenotes/deep_learning_course1/week2.html)
- [Week 3 (part 1) - Backpropagation derivation in shallow neural nets](/notes/coursenotes/deep_learning_course1/week3_part1.html)

---
