---
layout: page
title: About
permalink: /about/
---


<img src="/pics/profile_pic.png" width="250px"/>

I am a neuroscientist-turned-data scientist. 
I did my undergraduate degree at the University of Aberdeen, before moving to Newcastle
University for a PhD in 
computational and systems neuroscience. During my PhD, I spent two years at the 
National Institutes of Health just outside of Washington, DC in the US. My PhD work made use of
computational and 
machine learning techniques to identify shortcomings in our current understanding of 
visual neurons in the primate brain. I am very interested in the overlap between neuroscience
and artificial intelligence, as well as the impact of machine learning and automation on
employment and work. One of my focus areas is developing machine learning systems for solving
impactful
real-world problems, and I have recently been specialising in ML applications within financial
services.
