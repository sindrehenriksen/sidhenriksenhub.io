---
layout: post
title:  "Computing precision values corrected for downsampling"
date:   2018-06-11 21:00:00 +0000
categories: datascience
mathjax: true
---
Often when you are working on very large, imbalanced dataset it makes a huge amount of sense
downsample your majority class. For example, if you are working on predicting credit card fraud,
it doesn't necessarily matter whether you have 10,000 or 50,000 legitimate credit transactions
if you only have 30 fraudulent ones. For many problems, classification performance is
limited chiefly by the number of minority class observations you have. Under these
circumstances, you can radically speed up iteration time by downsampling your minority class.

The major issue is that you need account for how much you have downsampled. This in itself might
not be that straightforward, especially if you have a long, complex processing pipeline with
multiple people involved. Nevertheless, assuming that you have a good estimate of the class
imbalance in the full data as well as the downsampled data, computing a corrected precision
is a straightforward business. We'll first remind the reader that precision is defined as

$$
\newcommand{TP}{\mathrm{TP}}
\newcommand{FP}{\mathrm{FP}}
\newcommand{FN}{\mathrm{FN}}
\mathrm{precision} = \frac{\TP}{\TP + \FP},
$$

where TP and FP refer to true and false positives, respectively. We will assume that we have
the minority class proportion for the downsampled and original data, which we will denote as
\\(p_\mathrm{ds}\\) and \\(p_\mathrm{orig}\\). We define the downsampling factor as

$$
k = \frac{p_\mathrm{ds}}{p_\mathrm{orig}}.
$$

Now, this number tells us how much we have downsampled by. Intuitively, if
\\(p_\mathrm{ds} = 0.1\\) and \\(p_\mathrm{orig} = 0.01\\), then \\(k=10\\) meaning that
the class imbalance is now one tenth of what it used to be. This is key because when we
downsample the majority class, what we are effectively doing is reducing the total number of
false positives by a factor. In fact, this factor is the \\(k\\) we defined above.
We are now therefore in a position to compute the adjusted precision:

$$
\mathrm{precision}' = \frac{\TP}{\TP + k\FP}.
$$

If you ever do any sort of downsampling on the majority class, then this is the metric you want
to cite. Recall, on the other hand, does not change as you downsample since this only depends
on the minority class. 
