---
layout: post
title: "Getting a data science position in Scotland"
date: 2017-10-22 10:30:00 +0100
categories: datascience scotland
---

Having just gone through the process of getting a data scientist position, I
thought I’d document the process I went through and the particulars of data science
in Scotland. This is written from the perspective of a PhD graduate with a firm
grounding in machine learning, visualisation,
and analytics, but with limited industry experience.

![Edinburgh Castle from the Old Town](/pics/edinburgh_castle.png)	

# Location
The major cities all have different focuses and some might fit your particular
needs/interests better than others.
In Aberdeen, you’re likely to get a position in oil and gas; in Edinburgh, you’re more
likely to see jobs in financial
services, tech, or the public sector (e.g. health; both NHS National Services Scotland
and Healthcare Improvement Scotland are based in Edinburgh). Dundee is well-known for
its games industry, and I saw several
data science positions in games companies. Glasgow seems pretty diversified, and
has a fair amount of jobs in financial services, energy, and the public sector.
I encountered a few positions in Stirling which were
related to medical technology; Stirling is a small city with a large-ish university,
so you’re likely to find spin-out companies and similar. The same applies to St Andrews.
I really didn’t see anything data science-related in Inverness.

# Salary
The salary depends hugely on whether you work in the academic, public, or private sectors.
It can also vary greatly within sectors. In Edinburgh, a newly qualified postdoc makes
around £32,000 a year. My sense is that in the public sector, this is also around £30,000,
but increases sharply
with experience. A Senior Information Analyst with NHS Scotland earns between £26,000
and £35,000, and it’s unlikely that you will be put at the top end of the band starting
out. I have seen job postings for
experienced data scientists above £50,000 in the public sector. In the
private sector, salaries generally range
from £35,000 to £50,000 for recent PhD graduates. With a few years experience, this
increases a fair amount, often to £60,000+. Following your PhD, the difference
between a private sector job and an academic
(e.g. postdoc) or public sector position can easily be over £10,000 per annum. If you have
a master's degree and not a lot of experience, you probably have to scale it down accordingly.
Many private sector jobs also have generous benefits, such as high pension matching,
cash bonuses, private health insurance, and so on.

# The application
For private sector positions, the simplest thing is to register yourself with a
recruiter. For all their woes, recruiters really reduce a lot of the pain associated
with applying for jobs. Prepare a decent CV, have people look at it, and then send
it off to a recruitment agency. One of the
most active ones within data science in Scotland is MBN Solutions (Note: This is not
an ad; they are just by far the most active agency in data science and sponsor a lot
of data events). The recruiter will
then evaluate your CV and forward any job postings they think you're qualified for.
If you agree for your name to be put forward, they will then contact the company and
present you as a candidate. Prior to the
interview stage, there is very little work required of you. Note that recruiters work
on commission so have very mixed allegiances, especially as an offer is extended.
Recruiters want you to accept the offer
so that they can get close the sale, even if the offer is not the best they can do.

Startups, academia, and the public sector generally make little use of recruiters,
so for these jobs, you need to apply the old fashioned way. This is tedious, but
necessary for these types of jobs. There are many good resources for preparing data
science applications online so I won't go through that here.

# Interviews
The interviewing stage will vary from organisation to organisation. Some companies
will do an analytics challenge, where you get a dataset to poke and prod, and then
generally do some predictions. The point of these is largely to verify that you can
take a dataset, create visualisations, and extract some understanding of what's going
on in the data. Basically, it's a low effort way for companies to ensure that you're
not a fraud. It's worth bearing in mind that they require a lot of effort on behalf
of the applicant, so I would suggest only agreeing to do these for companies you're
really interested in. Otherwise, you'll find yourself pissing away your evenings
half-arsing challenges and most likely burn out. Quality over quantity is key here.

Most companies do a phone interview, lasting around 30 minutes. This is again mostly
for introductions and to ensure that you are at least the caricature of what they're
looking for. If successful, you'll then be invited for an in-person interview, lasting
around an hour. For data scientist hires, this will generally be someone pretty high
up in the company. For startups, expect to be interviewed by the CEO or CTO, and a
senior data scientist or developer. For large companies, you're probably looking at
the local head of analytics and/or a senior member of their analytics/data science
department.

Startups tend to have fairly relaxed interviews - bean bags and coffee. The interviews
are also generally unstructured, meaning that it's more of a conversation than a formal
interview. More emphasis is generally put on being a good "fit" than your capabilities
as a data scientist (although that matters too, of course). Large companies and the
public sector will often
do competency-based interviews in order to standardise interviews as much as possible.
Read up on those if you are
interviewing for a large company. These interviews suck because you get asked pre-made
questions about, for example, a time when you did something innovative. The problem is
that this is just not how information is organised in most people's heads (i.e. while
there might be a "stuff I did during my PhD" category, there is no "times I've done 
something innovative" category, at least that's how it is for me). The nice thing about
them is that they are often cooperative and not adversarial - the interviewers will
help you give a good answer as opposed to trying to grill you or catch you out. There
are loads of guides to competency-based interviews online which can be helpful.

# The offer
And then hopefully after all of that, you'll receive an offer. They will likely
have asked you about your current salary or your salary expectation during the process
(or you will have communicated this through your recruiter). Don't be afraid
to negotiate. You obviously don't want to come off as though you're being too difficult
to rein in, since they might just end up withdrawing the offer and extending it to another
applicant (especially if it's very little between you). But also don't be afraid to be
assertive, especially if you feel as though you're being low-balled. If the company is
low-balling you, it's a huge gamble for them to move on to another candidate since they
run the risk of ending up with no candidate at all, which would be disastrous (interviewing
candidates is both time-consuming and expensive). Also keep in mind that there are other things
you can negotiate than salary. For example, the amount of paid leave, conference allowance,
equipment allowance, or working hours are all things you could bring up at this point.