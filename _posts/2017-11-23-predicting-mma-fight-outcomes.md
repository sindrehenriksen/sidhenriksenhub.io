---
layout: post
title: "Predicting MMA fight outcomes"
date: 2017-11-23 07:30:00 +0100
categories: datascience
---

A while back I built a web crawler to scrape data from Fightmetric.com: this is a website which
contains information about MMA fighters, fights, etc. The idea was that I would build a prediction
engine
for predicting fight outcomes based on fighter stats. I finally got around to piecing this
work together into a presentable format.

<iframe src="https://sidhenriksen.com/apps/mma" width="90%" height="400px" frameBorder="0" scrolling="0">
</iframe>

This is an incredibly simple model: it tries to predict fight outcomes based on things like number of
wins and losses, the fighter's height, their weight, and so on. The limitation is that it does not
take into account how
many of those wins were against good opponents. In other words, the model has no way of
differentiating between a loss to a top 10 fighter, and a loss to a can. If you were to make this
work well enough to market it as a prediction engine or try to beat the bookies, that's the sort of
thing you would
need to implement. While I'm on the topic, don't use this to do anything stupid like betting. This
is just a bit of fun and should only be treated as such.


## Models and outcomes
I played around with a bunch of different models for this project (as I usually do), but
a simple logistic regression really worked as well as anything else I tried. In the app above
I am using an XGBoost model purely for aesthetic purposes (logistic regression tends to overshoot
its confidence for unlikely matchups [e.g. between different weight classes]). The fact that logistic
regression works really well here I think basically has to do with the
fact that MMA fights are incredibly stochastic: submissions or knockouts can come out of nowhere.
The features that are useful for predicting outcomes are usually pretty straightforward:
the more wins you have, the more likely you are to win your next fight; the more losses you have
the more likely you are to lose. We also have a few statisical measures I pulled from Fightmetric,
such as the percentage of takedown attempts the fighter prevents, though these are less strongly
related to the fight outcomes. We are unlikely to have incredibly complex features for this reason.
In plain terms, complex models allow us to construct scenarios such as "if weight
differential between two fighters is above 10% of their average bodyweight and the fighter is a 
southpaw,
then chance of a KO is 3 times higher than usual". This is a slightly contrived example, but I
think the principle is clear. Doing 5-fold cross-validation on this dataset, the model can get
about 75%-80% correct. See the long list of caveats below before getting too excited about this
number.

## The app
The code for the crawler and preprocessing the data is available on [Github](https://github.com/sidhenriksen/mma-prediction). The app is written in Dash and the code is
available in a separate [Github repository](https://github.com/sidhenriksen/mma-app).
More than anything I wanted an interactive way of sharing the results of the processing pipeline
and model since there was a fair amount of work that went into all of this.


## Caveats
The model can only learn to identify which features are
important for a fight outcome based on the data it sees. For example, most professional fights
take place between
two weight-matched and roughly skill-matched individuals. The consequence of that is that the model
never sees the sort of fights that should intuitively be one-sided. For example, plug in
Demetrious Johnson - the reigning flyweight champion and possibly the most technical fighter ever
in the UFC - against Stipe Miocic - the reigning heavyweight champion. Because the model very rarely
(if ever) sees a flyweight fight a heavyweight, it thinks that Demetrious Johnson is probably a
favourite to win this fight. DJ is probably my favourite fighter, but I don't think those are a fair
assessment of his chances against Stipe.

This highlights an issue with this model/dataset in particular, and a broader point about
machine learning in general: intuition is not built into our models. AI is very limited in their
ability to reason beyond the examples that we provide them. So in this case, our model might
be able to provide a good probabilistic assessment of fights that are likely to happen.

A related point is that most fights in the dataset are not high level fights between champions.
The majority of fights are between fighters of much more modest ability, and so the features
which predict fight outcomes for top fighters might not be the same as those which predict outcomes
for the majority of fights.

A final point is that this data is moderately leaky. What that means is that Fightmetric only stores
information about fighters at any given moment in time. In other words, I am trying to predict
whether Conor McGregor beat Dustin Poirier using the stats they have at present (actually, the
dataset is several months old, but you get the idea). Obviously, Conor McGregor's current stats
carry information about whether he won in the past. This is just a limitation of the data I have
access to, and is one of the key reasons why this can't be used as a proper prediction engine.