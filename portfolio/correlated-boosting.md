---
layout: page
exclude: true
---
<center>
<iframe src="https://sidhenriksen.com/apps/corrboosting" width="110%" height="1000px" frameBorder="0" scrolling='no'></iframe>
</center>

# The paper
This is a paper currently being prepared for peer review, and a version of this paper was the
penultimate chapter of my PhD thesis.
The key aim of the paper was to exhaustively test models of neurons
in an area of the brain known as primary visual cortex (or V1 for short).
These models -- known as linear-nonlinear models -- remain foundational to the field's thinking
about the computations performed by V1, and are very similar to the foundational units
that are used in modern neural networks. In order to test these models, we recorded from
cells in primary visual cortex, developed an extensive data pipeline to process >1TB data,
and used a machine learning framework for fitting the components of the model. In the visualisation
above, various metrics of these cells are shown on the horizontal and vertical axes, and the
disparity tuning curves are shown in the bottom panel for the cell (left, solid lines) and the
corresponding model (right, dashed lines). The accompanying paper will shortly be available for
download.

The code for this visualisation is [available on Github](https://github.com/sidhenriksen/corrboosting_viz).